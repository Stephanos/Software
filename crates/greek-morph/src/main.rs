use unicode_normalization::{char::decompose_canonical, UnicodeNormalization};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
enum WordType {
	#[serde(rename = "Substantiv")]
	Substantive,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct DictEntry {
	#[serde(rename = "Wort")]
	word: String,
	#[serde(rename = "Bedeutung")]
	meaning: String,
	#[serde(rename = "Typ")]
	word_type: WordType,
}

#[derive(Debug)]
struct Word {
	regular: String,
	raw: String,
	stem: String,
	raw_stem: String,
}

impl From<DictEntry> for Word {
	fn from(entry: DictEntry) -> Self {
		let regular = entry.word;
		let raw = to_raw(&regular);
		let stem = todo!();
		let raw_stem = todo!();

		Self {
			regular,
			raw: todo!(),
			stem: todo!(),
			raw_stem: todo!(),
		}
	}
}

const SPIRITUS_ASPER: char = '\u{314}';
const SPIRITUS_LENIS: char = '\u{313}';

fn to_raw(string: &str) -> String {
	let mut out = String::new();
	for c in string.nfc() {
		let mut first = true;

		decompose_canonical(c, |c| {
			dbg!(&c);
			if first || matches!(c, SPIRITUS_ASPER | SPIRITUS_LENIS) {
				out.push(c);
			}
			first = false;
		});
	}
	dbg!(out.nfc().collect())
}

fn main() {
	let dict_content = std::fs::read_to_string("../dict.yaml").unwrap();

	let entries: Vec<DictEntry> = serde_yaml::from_str(&dict_content).unwrap();
	dbg!(&entries);

	let words: Vec<Word> = entries.into_iter().map(Word::from).collect();
}
