use anyhow::Result;
use std::path::PathBuf;

use crate::bvc::Bvc;

#[derive(Debug, serde::Deserialize)]
struct CsvRecord {
	#[serde(rename = "BCV")]
	bcv: String,
	text: String,
	func_morph: String,
	form_morph: String,
	strongs: String,
	lemma: String,
	normalized: String,
}

pub fn read_greek_csv(path: PathBuf) -> Result<Vec<Record>> {
	let mut reader = csv::ReaderBuilder::new()
		.delimiter(b'\t')
		.trim(csv::Trim::All)
		.comment(Some(b'#'))
		.from_path(path)?;

	reader
		.deserialize::<CsvRecord>()
		.map(|record| {
			record
				.map_err(anyhow::Error::from)
				.and_then(Record::try_from)
		})
		.collect()
}

#[derive(Debug, Clone)]
pub struct Record {
	pub bcv: Bvc,
	pub text: String,
	pub func_morph: String,
	pub form_morph: String,
	pub strongs: String,
	pub lemma: String,
	pub normalized: String,
}

impl TryFrom<CsvRecord> for Record {
	type Error = anyhow::Error;

	fn try_from(value: CsvRecord) -> Result<Self> {
		let CsvRecord {
			bcv,
			text,
			func_morph,
			form_morph,
			strongs,
			lemma,
			normalized,
		} = value;

		let bcv = Bvc::parse(&bcv)?;

		Ok(Self {
			bcv,
			text,
			func_morph,
			form_morph,
			strongs,
			lemma,
			normalized,
		})
	}
}
