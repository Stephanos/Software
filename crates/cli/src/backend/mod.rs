pub mod typst;

pub trait Backend {
	fn compile() -> String;
}
