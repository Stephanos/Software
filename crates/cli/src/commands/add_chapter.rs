use anyhow::{anyhow, Result};
use std::{fs, io::Read, path::PathBuf};

use crate::{bvc::Bvc, dict::Dict, text::TextStorage};

use super::add_verse::add_verse_internal;

pub fn add_chapter(dict_path: PathBuf, csv_path: PathBuf, chapter: String) -> Result<()> {
	let bc = Bvc::parse_bc(&chapter)?;
	let dict = Dict::from_file(&dict_path);
	let records = crate::csv::read_greek_csv(csv_path)?;
	let storage = TextStorage::from_csv(records);

	let chapter = storage.lookup_bc(&bc)?;

	let path = bc.md_path();
	let mut file = fs::OpenOptions::new().read(true).open(path)?;

	let mut content = String::new();
	file.read_to_string(&mut content)?;

	let existing_verses: Vec<usize> = content
		.lines()
		.filter_map(|line| {
			line.starts_with("##")
				.then(|| line.trim_start_matches("##").trim().parse::<usize>().ok())
				.flatten()
				.and_then(|verse_num| verse_num.checked_sub(1))
		})
		.collect();

	for verse_idx in 0..chapter.verses.len() {
		if !existing_verses.contains(&verse_idx) {
			let bcv = bc.with_verse(verse_idx as u8);
			if let Err(err) = add_verse_internal(&dict, &storage, &bcv) {
				return Err(anyhow!("in verse `{:}`:\n{err}", bcv.verse()));
			}

			println!("> Added verse `{:}`", bcv.verse());
		}
	}

	println!("> CHAPTER COMPLETE!");
	Ok(())
}
