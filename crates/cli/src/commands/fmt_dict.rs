use std::path::PathBuf;

use dict::Dict;

pub fn fmt_dict(dict_path: PathBuf) -> anyhow::Result<()> {
	Dict::format(dict_path)
}
