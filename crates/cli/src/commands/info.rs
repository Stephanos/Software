use std::path::PathBuf;

use crate::{dict::Dict, text, util};

pub fn info(dict_path: PathBuf, csv_path: PathBuf, verse: String, range: Option<String>) {
	let dict = Dict::from_file(&dict_path);

	let (start, end) = if let Some(range) = range {
		if let Some((start, end)) = range.split_once('-') {
			(
				start.parse().expect("Invalid range"),
				end.parse().expect("Invalid range"),
			)
		} else {
			let index = range.parse().expect("Invalid range");
			(index, index)
		}
	} else {
		(0, usize::MAX)
	};

	let records = crate::csv::read_greek_csv(csv_path).unwrap();
	let storage = text::TextStorage::from_csv(records);
	let verse = storage.lookup_verse(&verse).unwrap();

	for (idx, word) in verse.words.into_iter().enumerate() {
		// For human numbers, starting from 1
		let idx = idx + 1;
		if idx < start || idx > end {
			continue;
		}

		let lookup = dict.lookup(&word.lemma).unwrap();

		println!(
			"{idx:>2}: {} | {}-{}",
			word.text, word.lemma, word.func_morph
		);

		print!("    {}", util::intersperse(&lookup.meanings));

		if !lookup.comment.is_empty() {
			print!(" # {}", lookup.comment)
		}
		println!();
		println!();
	}
}
