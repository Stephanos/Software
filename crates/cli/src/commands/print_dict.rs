use std::path::PathBuf;

use anyhow::Result;
use dict::Dict;

pub fn print_dict(dict_path: PathBuf) -> Result<()> {
	let dict = Dict::from_file(&dict_path)?;

	println!("{dict:#?}");

	Ok(())
}
