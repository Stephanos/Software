use anyhow::Result;
use std::{
	fmt::Write as _,
	fs,
	io::{Read, Seek, Write},
	path::PathBuf,
};

use crate::{bvc::Bvc, dict::Dict, text::TextStorage};

pub fn add_verse(dict_path: PathBuf, csv_path: PathBuf, verse: String) -> Result<()> {
	let bcv = Bvc::parse(&verse)?;

	let dict = Dict::from_file(&dict_path);

	let records = crate::csv::read_greek_csv(csv_path)?;
	let storage = TextStorage::from_csv(records);

	add_verse_internal(&dict, &storage, &bcv)
}

pub fn add_verse_internal(dict: &Dict, storage: &TextStorage, bcv: &Bvc) -> Result<()> {
	let verse = storage.lookup_bcv(bcv)?;

	let path = bcv.md_path();
	let mut file = fs::OpenOptions::new()
		.read(true)
		.write(true)
		.create(true)
		.truncate(true)
		.open(path)?;

	let mut content = String::new();
	file.read_to_string(&mut content)?;
	let mut lines: Vec<String> = content.lines().map(ToOwned::to_owned).collect();

	let expected_heading = format!("## {}", bcv.verse());
	let (insert_idx, insert_heading) = if let Some(pos) = lines
		.iter()
		.position(|line| line.starts_with(&expected_heading))
	{
		(pos + 1, false)
	} else {
		(lines.len(), true)
	};

	let text: String =
		verse
			.words
			.into_iter()
			.enumerate()
			.try_fold(String::new(), |mut text, (idx, word)| {
				// For human numbers, starting from 1
				let idx = idx + 1;
				let lookup = dict.lookup_word(&word.lemma)?;

				let (start_paren, greek_word, punctuation) = word.split_punctuation();

				write!(
					text,
					"{start_paren}[{lookup}]({greek_word}-{}-{idx}){punctuation} ",
					word.form_morph
				)
				.unwrap();
				Ok::<String, anyhow::Error>(text)
			})?;

	lines.insert(insert_idx, text);
	lines.insert(insert_idx, "".into());

	if insert_heading {
		lines.insert(insert_idx, expected_heading);
		lines.insert(insert_idx, "".into());
	}

	let new_content: String = lines.into_iter().fold(String::new(), |mut out, line| {
		writeln!(&mut out, "{line}").unwrap();
		out
	});

	file.rewind()?;
	file.write_all(new_content.as_bytes())?;

	Ok(())
}
