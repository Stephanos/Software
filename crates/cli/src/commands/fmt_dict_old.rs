use std::{fmt::Write, path::PathBuf};

use unicode_normalization::{char::decompose_canonical, UnicodeNormalization};

use crate::{dict::Dict, util};

pub(crate) fn fmt_dict(dict_path: PathBuf) {
	let mut dict = Dict::from_file(&dict_path).into_vec();
	dict.sort_by(|(word, _), (other, _)| {
		let word = strip_accents(word);
		let other = strip_accents(other);
		word.cmp(&other)
	});

	let mut out = String::new();
	let mut current_char = strip_accents(&dict[0].0).chars().next().unwrap();
	for (word, info) in dict {
		let first_char = strip_accents(&word).chars().next().unwrap();
		if first_char != current_char {
			current_char = first_char;
			writeln!(&mut out).unwrap();
		}

		let comment = if info.comment.is_empty() {
			"".into()
		} else {
			format!(" # {}", info.comment)
		};

		writeln!(
			&mut out,
			"{word} = {}{comment}",
			util::intersperse(&info.meanings)
		)
		.unwrap();
	}

	std::fs::write(dict_path, out).unwrap();
}

fn strip_accents(string: &str) -> String {
	string
		.nfc()
		.map(|c| {
			// Only take the first char
			let mut base_char = None;
			decompose_canonical(c, |c| {
				base_char.get_or_insert(c);
			});
			base_char.unwrap()
		})
		.collect()
}
