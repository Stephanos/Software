use std::path::PathBuf;

use clap::{Parser, Subcommand};

#[derive(Subcommand, Debug)]
pub enum Commands {
	/// Add a verse to the translation document
	AddVerse {
		/// Name of the verse, e.g. "John 1:1"
		verse: String,
	},
	/// Add a whole chapter to the translation document
	AddChapter {
		/// Name of the chapter, e.g. "John 1"
		chapter: String,
	},
	/// Print the info about a verse
	Info {
		/// Name of the verse, e.g. "John 1:1"
		verse: String,
		/// Word index or range, e.g. "1" or "1-3"
		range: Option<String>,
	},
	/// Format dictionary file
	FmtDict,
	/// Print the dictionary
	PrintDict,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
	#[command(subcommand)]
	pub command: Commands,

	/// Sets a custom config file
	#[arg(short, long, value_name = "FILE")]
	pub csv: Option<PathBuf>,

	/// Sets a custom dictionary file
	#[arg(short, long, value_name = "FILE")]
	pub dict: Option<PathBuf>,
}
