use crate::{bvc::Bvc, csv::Record};
use anyhow::{anyhow, Result};
use std::collections::HashMap;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Debug, Clone)]
pub struct TextStorage {
	pub books: HashMap<String, Book>,
}

#[derive(Debug, Clone)]
pub struct Book {
	pub chapters: Vec<Chapter>,
}

#[derive(Debug, Clone)]
pub struct Chapter {
	pub verses: Vec<Verse>,
}

#[derive(Debug, Clone)]
pub struct Verse {
	pub words: Vec<Word>,
}

#[derive(Debug, Clone)]
pub struct Word {
	pub text: String,
	pub func_morph: String,
	pub form_morph: String,
	pub strongs: String,
	pub lemma: String,
	pub normalized: String,
}

impl From<&Record> for Word {
	fn from(value: &Record) -> Self {
		let Record {
			text,
			func_morph,
			form_morph,
			strongs,
			lemma,
			normalized,
			..
		} = value.clone();

		Self {
			text,
			func_morph,
			form_morph,
			strongs,
			lemma,
			normalized,
		}
	}
}

impl TextStorage {
	pub fn from_csv(records: Vec<Record>) -> Self {
		let mut book_name = &records[0].bcv.book;
		let mut start_idx = 0;
		let mut books = HashMap::new();
		for (idx, record) in records.iter().enumerate() {
			if book_name != &record.bcv.book {
				books.insert(
					book_name.clone(),
					Book::from_records(&records[start_idx..idx]),
				);
				book_name = &record.bcv.book;
				start_idx = idx;
			}
		}
		books.insert(book_name.clone(), Book::from_records(&records[start_idx..]));

		Self { books }
	}

	pub fn lookup_verse(&self, verse_bcv: &str) -> Result<Verse> {
		let bvc = Bvc::parse(verse_bcv)?;
		self.lookup_bcv(&bvc)
	}

	pub fn lookup_bcv(&self, bcv: &Bvc) -> Result<Verse> {
		self.books
			.get(&bcv.book)
			.ok_or_else(|| anyhow!("Couldn't find book `{}`", bcv.book))?
			.chapters
			.get(bcv.chapter_idx as usize)
			.ok_or_else(|| {
				anyhow!(
					"Couldn't find chapter `{}` of book `{}`",
					bcv.chapter(),
					bcv.book
				)
			})?
			.verses
			.get(bcv.verse_idx as usize)
			.cloned()
			.ok_or_else(|| {
				anyhow!(
					"Couldn't find verse `{}` in chapter `{}` of book `{}`",
					bcv.verse(),
					bcv.chapter(),
					bcv.book
				)
			})
	}

	pub fn lookup_bc(&self, bc: &Bvc) -> Result<Chapter> {
		self.books
			.get(&bc.book)
			.ok_or_else(|| anyhow!("Couldn't find book `{}`", bc.book))?
			.chapters
			.get(bc.chapter_idx as usize)
			.ok_or_else(|| {
				anyhow!(
					"Couldn't find chapter `{}` of book `{}`",
					bc.chapter(),
					bc.book
				)
			})
			.cloned()
	}
}

impl Book {
	fn from_records(records: &[Record]) -> Self {
		let mut chapter_idx = records[0].bcv.chapter_idx;
		let mut start_idx = 0;
		let mut chapters = Vec::new();
		for (idx, record) in records.iter().enumerate() {
			if chapter_idx != record.bcv.chapter_idx {
				chapters.push(Chapter::from_records(&records[start_idx..idx]));
				chapter_idx = record.bcv.chapter_idx;
				start_idx = idx;
			}
		}
		chapters.push(Chapter::from_records(&records[start_idx..]));

		Self { chapters }
	}
}

impl Chapter {
	fn from_records(records: &[Record]) -> Self {
		let mut verse_idx = records[0].bcv.verse_idx;
		let mut start_idx = 0;
		let mut verses = Vec::new();
		for (idx, record) in records.iter().enumerate() {
			if verse_idx != record.bcv.verse_idx {
				verses.push(Verse::from_records(&records[start_idx..idx]));
				verse_idx = record.bcv.verse_idx;
				start_idx = idx;
			}
		}
		verses.push(Verse::from_records(&records[start_idx..]));

		Self { verses }
	}
}

impl Verse {
	fn from_records(records: &[Record]) -> Self {
		let verses = records.iter().map(Word::from).collect();

		Self { words: verses }
	}
}
impl Word {
	/// Removes punctuation from the word by using the length of the normalized version.
	/// Returns the greek word without punctuation first and then the punctuation.
	pub(crate) fn split_punctuation(&self) -> (String, String, String) {
		let normalized_len = self.normalized.graphemes(true).count();

		let start_paren = if self.text.starts_with('(') {
			"(".into()
		} else {
			String::new()
		};

		let text = self.text.trim_start_matches('(');
		let mut greek_word = String::new();
		let mut punctuation = String::new();

		for (idx, c) in text.graphemes(true).enumerate() {
			if idx < normalized_len {
				greek_word.push_str(c);
			} else {
				punctuation.push_str(c);
			}
		}

		(start_paren, greek_word, punctuation)
	}
}
