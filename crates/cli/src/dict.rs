use anyhow::{anyhow, Result};
use std::{collections::HashMap, path::Path};

#[derive(Debug, Clone)]
pub(crate) struct DictWord {
	pub(crate) meanings: Vec<String>,
	pub(crate) comment: String,
}

#[derive(Debug)]
pub(crate) struct Dict {
	data: HashMap<String, DictWord>,
}

impl Dict {
	pub(crate) fn from_file<P: AsRef<Path>>(p: &P) -> Self {
		let string = std::fs::read_to_string(p).unwrap();
		let data = string
			.lines()
			.filter_map(|line| {
				if line.trim().is_empty() {
					None
				} else {
					let (word, info) = line.split_once('=').unwrap();

					let word = word.trim();

					let (meanings, comment) =
						if let Some((meanings, comment)) = info.split_once('#') {
							(meanings, comment.trim().to_owned())
						} else {
							(info, "".to_owned())
						};

					let meanings = meanings
						.split(',')
						.map(str::trim)
						.map(ToOwned::to_owned)
						.inspect(|meaning| {
							if meaning.is_empty() {
								println!("Word `{word}` has empty meaning");
							}
						})
						.collect();

					Some((word.to_owned(), DictWord { meanings, comment }))
				}
			})
			.collect();

		Self { data }
	}

	pub(crate) fn lookup(&self, word: &str) -> Result<DictWord> {
		let word = word.to_lowercase();
		let result = self
			.data
			.get(&word)
			.ok_or_else(|| anyhow!("Couldn't find word `{word}` in dictionary"))?;
		Ok(result.clone())
	}

	pub(crate) fn lookup_word(&self, word: &str) -> Result<String> {
		Ok(self.lookup(word)?.meanings.into_iter().next().unwrap())
	}

	pub(crate) fn into_vec(self) -> Vec<(String, DictWord)> {
		self.data.into_iter().collect()
	}
}
