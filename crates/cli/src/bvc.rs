use anyhow::{anyhow, Result};

#[derive(Debug, Clone)]
pub(crate) struct Bvc {
	pub(crate) book: String,
	pub(crate) chapter_idx: u8,
	pub(crate) verse_idx: u8,
}

impl Bvc {
	fn parse_internal(bcv: &str, parse_verse: bool) -> Result<Bvc> {
		let (book, numbers) = bcv.split_once(' ').unwrap_or_default();
		let (chapter, verse) = if parse_verse {
			numbers.split_once(':').unwrap_or_default()
		} else {
			(numbers, "")
		};

		let chapter_idx = chapter
			.parse::<u8>()?
			.checked_sub(1)
			.ok_or_else(|| anyhow!("Chapter numbers start at 1"))?;
		let verse_idx = if parse_verse {
			verse
				.parse::<u8>()?
				.checked_sub(1)
				.ok_or_else(|| anyhow!("Verse numbers start at 1"))?
		} else {
			0
		};

		Ok(Self {
			book: book.to_owned(),
			chapter_idx,
			verse_idx,
		})
	}

	pub(crate) fn parse(bcv: &str) -> Result<Bvc> {
		Self::parse_internal(bcv, true)
	}

	pub(crate) fn parse_bc(bcv: &str) -> Result<Bvc> {
		Self::parse_internal(bcv, false)
	}

	pub(crate) fn md_path(&self) -> String {
		format!(
			"text/{}/{}.md",
			self.book.trim().to_lowercase(),
			self.chapter()
		)
	}

	#[must_use]
	pub(crate) fn with_verse(&self, verse_idx: u8) -> Self {
		Self {
			book: self.book.clone(),
			chapter_idx: self.chapter_idx,
			verse_idx,
		}
	}

	pub(crate) fn chapter(&self) -> u8 {
		self.chapter_idx + 1
	}

	pub(crate) fn verse(&self) -> u8 {
		self.verse_idx + 1
	}
}
