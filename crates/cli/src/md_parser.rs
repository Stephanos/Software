use anyhow::Result;
use std::path::Path;

pub(crate) struct MdParser {
	pub(crate) verses: Vec<MdVerse>,
}

pub(crate) struct MdVerse {
	pub(crate) words: Vec<MdWord>,
}

pub(crate) struct MdWord {
	pub(crate) german: String,
	pub(crate) greek: String,
}

impl MdParser {
	pub(crate) fn new<P: AsRef<Path>>(p: &P) -> Result<Self> {
		todo!()
	}
}
