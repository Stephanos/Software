use std::fmt::Write;

pub(crate) fn intersperse(strings: &[String]) -> String {
	if strings.len() == 1 {
		strings[0].to_string()
	} else {
		let mut out = String::new();
		write!(&mut out, "{}", strings[0]).unwrap();
		for string in strings.iter().skip(1) {
			write!(&mut out, ", {string}").unwrap();
		}
		out
	}
}
