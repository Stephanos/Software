use anyhow::Result;
use std::{path::PathBuf, str::FromStr};
use stephanos_cli::{cli, commands};

use clap::Parser;

fn main() -> Result<()> {
	let args = cli::Cli::parse();
	let csv_path = args
		.csv
		.unwrap_or_else(|| PathBuf::from_str("text/revised_Nestle1904.csv").unwrap());

	let dict_path = args
		.dict
		.unwrap_or_else(|| PathBuf::from_str("dict").unwrap());

	match args.command {
		cli::Commands::AddVerse { verse } => {
			commands::add_verse(dict_path, csv_path, verse)?;
		}
		cli::Commands::AddChapter { chapter } => {
			commands::add_chapter(dict_path, csv_path, chapter)?;
		}
		cli::Commands::Info { verse, range } => {
			commands::info(dict_path, csv_path, verse, range);
		}
		cli::Commands::FmtDict => {
			commands::fmt_dict(dict_path)?;
		}
		cli::Commands::PrintDict => {
			commands::print_dict(dict_path)?;
		}
	}
	Ok(())
}
