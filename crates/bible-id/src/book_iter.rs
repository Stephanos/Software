use crate::{BookId, BookIter};

impl Default for BookIter {
	fn default() -> Self {
		Self {
			idx: 0,
			back_idx: BookId::num_of_books(),
		}
	}
}

impl Iterator for BookIter {
	type Item = BookId;

	fn next(&mut self) -> Option<Self::Item> {
		self.has_elements()?;
		let book = BookId::try_from_book_idx(self.idx);
		self.idx = self.idx.saturating_add(1);
		book.ok()
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		(self.len(), Some(self.len()))
	}
}

impl DoubleEndedIterator for BookIter {
	fn next_back(&mut self) -> Option<Self::Item> {
		self.has_elements()?;
		self.back_idx = self.back_idx.saturating_sub(1);
		let book = BookId::try_from_book_idx(self.back_idx);
		book.ok()
	}
}

impl ExactSizeIterator for BookIter {
	fn len(&self) -> usize {
		self.back_idx.saturating_sub(self.idx) as usize
	}
}

impl BookIter {
	#[must_use]
	pub fn new() -> Self {
		Self::default()
	}

	pub fn at() -> std::iter::Take<Self> {
		Self::default().take(39)
	}

	pub fn nt() -> std::iter::Skip<Self> {
		Self::default().skip(39)
	}

	fn has_elements(&self) -> Option<()> {
		(self.len() != 0).then_some(())
	}
}

#[cfg(test)]
mod test {
	use crate::{BookId, BookIter};

	#[test]
	fn test_whole_iter() {
		let iter = BookIter::new();
		assert_eq!(iter.len(), 66);
		assert_eq!(iter.count(), 66);
		let mut iter = BookIter::new();
		assert_eq!(iter.next(), Some(BookId::Gen));
		assert_eq!(iter.next_back(), Some(BookId::Rev));
		assert_eq!(iter.next(), Some(BookId::Exo));
		assert_eq!(iter.nth(59), Some(BookId::Jn3));
		assert_eq!(iter.nth_back(2), Some(BookId::Heb));
		assert_eq!(iter.next(), None);
		assert_eq!(iter.next_back(), None);
	}

	#[test]
	fn test_at_iter() {
		let iter = BookIter::at();
		assert_eq!(iter.len(), 39);
		assert_eq!(iter.count(), 39);
		let mut iter = BookIter::at();
		assert_eq!(iter.next(), Some(BookId::Gen));
		assert_eq!(iter.last(), Some(BookId::Mal));
	}

	#[test]
	fn test_nt_iter() {
		let iter = BookIter::nt();
		assert_eq!(iter.len(), 27);
		assert_eq!(iter.count(), 27);
		let mut iter = BookIter::nt();
		assert_eq!(iter.next(), Some(BookId::Mat));
		assert_eq!(iter.last(), Some(BookId::Rev));
	}
}
