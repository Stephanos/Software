mod book;
mod book_iter;
mod structure;

pub use structure::BIBLE_STRUCTURE;

#[derive(
	Debug,
	Clone,
	Copy,
	serde::Serialize,
	serde::Deserialize,
	PartialEq,
	Eq,
	PartialOrd,
	Ord,
	num_enum::TryFromPrimitive,
	num_enum::IntoPrimitive,
)]
#[serde(rename_all = "UPPERCASE")]
#[repr(u8)]
pub enum BookId {
	/// 1. Mose
	#[serde(alias = "1. Mose", alias = "1. MOS")]
	Gen,
	/// 2. Mose
	#[serde(alias = "2. Mose", alias = "2. MOS")]
	Exo,
	/// 3. Mose
	#[serde(alias = "3. Mose", alias = "3. MOS")]
	Lev,
	/// 4. Mose
	#[serde(alias = "4. Mose", alias = "4. MOS")]
	Num,
	/// 5. Mose
	#[serde(alias = "5. Mose", alias = "5. MOS")]
	Deu,
	/// Josua
	#[serde(alias = "Josua")]
	Jos,
	/// Richter
	#[serde(alias = "Richter")]
	Jdg,
	/// Rut
	#[serde(alias = "Rut")]
	Rut,
	/// 1. Samuel
	#[serde(rename = "1SA", alias = "1. Samuel")]
	Sa1,
	/// 2. Samuel
	#[serde(rename = "2SA", alias = "2. Samuel")]
	Sa2,
	/// 1. Könige
	#[serde(rename = "1KI", alias = "1. Könige")]
	Ki1,
	/// 2. Könige
	#[serde(rename = "2KI", alias = "2. Könige")]
	Ki2,
	/// 1. Chronik
	#[serde(rename = "1CH", alias = "1. Chronik")]
	Ch1,
	/// 2. Chronik
	#[serde(rename = "2CH", alias = "2. Chronik")]
	Ch2,
	/// Esra
	#[serde(alias = "Esra")]
	Ezr,
	/// Nehemia
	#[serde(alias = "Nehemia")]
	Neh,
	/// Ester
	#[serde(alias = "Ester")]
	Est,
	/// Hiob
	#[serde(alias = "Hiob")]
	Job,
	/// Psalm
	#[serde(alias = "Psalm")]
	Psa,
	/// Sprüche
	#[serde(alias = "Sprüche")]
	Pro,
	/// Prediger
	#[serde(alias = "Prediger")]
	Ecc,
	/// Hoheslied
	#[serde(alias = "Hoheslied")]
	Sng,
	/// Jesaja
	#[serde(alias = "Jesaja")]
	Isa,
	/// Jeremia
	#[serde(alias = "Jeremia")]
	Jer,
	/// Klagelieder
	#[serde(alias = "Klagelieder")]
	Lam,
	/// Hesekiel
	#[serde(alias = "Hesekiel")]
	Ezk,
	/// Daniel
	#[serde(alias = "Daniel")]
	Dan,
	/// Hosea
	#[serde(alias = "Hosea")]
	Hos,
	/// Joel
	#[serde(alias = "Joel")]
	Jol,
	/// Amos
	#[serde(alias = "Amos")]
	Amo,
	/// Obadja
	#[serde(alias = "Obadja")]
	Oba,
	/// Jona
	#[serde(alias = "Jona")]
	Jon,
	/// Micha
	#[serde(alias = "Micha")]
	Mic,
	/// Nahum
	#[serde(alias = "Nahum")]
	Nam,
	/// Habakuk
	#[serde(alias = "Habakuk")]
	Hab,
	/// Zefanja
	#[serde(alias = "Zefanja")]
	Zep,
	/// Haggai
	#[serde(alias = "Haggai")]
	Hag,
	/// Sacharja
	#[serde(alias = "Sacharja")]
	Zec,
	/// Maleachi
	#[serde(alias = "Maleachi")]
	Mal,
	/// Matthäus
	#[serde(alias = "Matthäus")]
	Mat,
	/// Markus
	#[serde(alias = "Markus")]
	Mrk,
	/// Lukas
	#[serde(alias = "Lukas")]
	Luk,
	/// Johannes
	#[serde(alias = "Johannes")]
	Jhn,
	/// Apostelgeschichte
	#[serde(alias = "Apostelgeschichte")]
	Act,
	/// Römer
	#[serde(alias = "Römer")]
	Rom,
	/// 1. Korinther
	#[serde(rename = "1CO", alias = "1. Korinther")]
	Co1,
	/// 2. Korinther
	#[serde(rename = "2CO", alias = "2. Korinther")]
	Co2,
	/// Galater
	#[serde(alias = "Galater")]
	Gal,
	/// Epheser
	#[serde(alias = "Epheser")]
	Eph,
	/// Philipper
	#[serde(alias = "Philipper")]
	Php,
	/// Kolosser
	#[serde(alias = "Kolosser")]
	Col,
	/// 1. Thessalonicher
	#[serde(rename = "1TH", alias = "1. Thessalonicher")]
	Th1,
	/// 2. Thessalonicher
	#[serde(rename = "2TH", alias = "2. Thessalonicher")]
	Th2,
	/// 1. Timotheus
	#[serde(rename = "1TI", alias = "1. Timotheus")]
	Ti1,
	/// 2. Timotheus
	#[serde(rename = "2TI", alias = "2. Timotheus")]
	Ti2,
	/// Titus
	#[serde(alias = "Titus")]
	Tit,
	/// Philemon
	#[serde(alias = "Philemon")]
	Phm,
	/// 1. Petrus
	#[serde(rename = "1PE", alias = "1. Petrus")]
	Pe1,
	/// 2. Petrus
	#[serde(rename = "2PE", alias = "2. Petrus")]
	Pe2,
	/// 1. Johannes
	#[serde(rename = "1JN", alias = "1. Johannes")]
	Jn1,
	/// 2. Johannes
	#[serde(rename = "2JN", alias = "2. Johannes")]
	Jn2,
	/// 3. Johannes
	#[serde(rename = "3JN", alias = "3. Johannes")]
	Jn3,
	/// Hebräer
	#[serde(alias = "Hebräer")]
	Heb,
	/// Jakobus
	#[serde(alias = "Jakobus")]
	Jas,
	/// Judas
	#[serde(alias = "Judas")]
	Jud,
	/// Offenbarung
	#[serde(alias = "Offenbarung")]
	Rev,
}

#[derive(Debug)]
pub struct BookIter {
	idx: u8,
	back_idx: u8,
}

#[derive(Debug)]
pub struct ChapterId {
	book: BookId,
	chapter: u8,
}

#[derive(Debug)]
pub struct VerseId {
	chapter: ChapterId,
	verse: u8,
}
