use serde::{de::value::StrDeserializer, Serialize};

use crate::{BookId, BIBLE_STRUCTURE};

impl std::fmt::Display for BookId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.serialize(f)
	}
}

impl BookId {
	/// Returns the number of books.
	pub fn num_of_books() -> u8 {
		BIBLE_STRUCTURE.len() as u8
	}

	/// Returns the numerical index of this book.
	#[must_use]
	pub fn to_book_idx(&self) -> u8 {
		*self as u8
	}

	/// Returns the numerical index of this book.
	pub fn try_from_book_idx(idx: u8) -> Result<Self, num_enum::TryFromPrimitiveError<Self>> {
		Self::try_from(idx)
	}

	#[must_use]
	pub fn num_of_chapters(&self) -> u8 {
		self.chapter_stats().len() as u8
	}

	#[must_use]
	pub fn chapter_stats(&self) -> &'static [u8] {
		&BIBLE_STRUCTURE[self.to_book_idx() as usize]
	}

	pub fn try_from_str(value: &str) -> Result<Self, serde::de::value::Error> {
		let de: StrDeserializer<'_, _> = StrDeserializer::new(value);
		serde::de::Deserialize::deserialize(de)
	}
}
