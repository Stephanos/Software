#![feature(btree_cursors)]

use simsearch::SimSearch;
use std::{collections::BTreeMap, ops::RangeInclusive};

mod load;
mod lookup;
mod page_estimation;

mod format;
#[cfg(test)]
mod test;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum WordType {
	Substantiv,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Meaning {
	#[serde(rename = "Bedeutung")]
	pub meaning: String,
	#[serde(rename = "Kommentar", skip_serializing_if = "Option::is_none")]
	pub comment: Option<String>,
	#[serde(rename = "Bücher", skip_serializing_if = "Option::is_none")]
	pub books: Option<Vec<String>>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct DictEntry {
	#[serde(rename = "Wort")]
	pub word: String,
	#[serde(rename = "Bedeutungen")]
	pub meanings: Vec<Meaning>,
	#[serde(rename = "Typ")]
	pub word_type: WordType,
	#[serde(rename = "Strong")]
	pub strong: u16,
	#[serde(rename = "Gemoll")]
	pub gemoll: u16,
	#[serde(rename = "Komplex", default, skip_serializing_if = "std::ops::Not::not")]
	pub complex: bool,
	#[serde(rename = "Kommentar", skip_serializing_if = "Option::is_none")]
	pub comment: Option<String>,
}

#[derive(Debug)]
struct Word {
	regular: String,
	raw: String,
	stem: String,
	raw_stem: String,
}

#[derive(Debug, Clone)]
pub struct DictWord {
	pub(crate) meanings: Vec<String>,
	pub(crate) comment: String,
}

/// A type storing the information about a dictionary.
///
/// You can lookup word by using their default form or
/// strong number.
/// You can also search for words using greek letters or
/// latin letters.
#[derive(Debug, Default)]
pub struct Dict {
	words: Vec<DictEntry>,
	strong_indices: Vec<RangeInclusive<usize>>,
	greek_indices_map: BTreeMap<String, usize>,
	greek_indices: SimSearch<usize>,
	latin_indices: SimSearch<usize>,
}
