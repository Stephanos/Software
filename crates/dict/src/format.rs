use std::path::{Path, PathBuf};

use anyhow::Context;
use text_util::GrWord;

use crate::{Dict, DictEntry};

#[derive(Debug)]
struct DictError {
	path: PathBuf,
	error: serde_yaml::Error,
	content: String,
}

impl DictError {
	fn new(path: PathBuf, error: serde_yaml::Error, content: String) -> Self {
		Self {
			path,
			error,
			content,
		}
	}
}

impl std::error::Error for DictError {}

impl std::fmt::Display for DictError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let location_info = if let Some(location) = self.error.location() {
			let line_nr = location.line();
			let column_nr = location.column();
			let relevant_line = self.content.lines().nth(line_nr).unwrap();
			let line_nr_str_len = line_nr.to_string().len();
			format!(
				"{line_nr} | {relevant_line}\n{} ={}^",
				" ".repeat(line_nr_str_len),
				"-".repeat(column_nr)
			)
		} else {
			"".into()
		};

		write!(
			f,
			"{} in file `{:?}`\n{location_info}",
			self.error, self.path
		)
	}
}

impl Dict {
	pub fn format<P: AsRef<Path>>(p: P) -> anyhow::Result<()> {
		let path = p.as_ref();

		if path.is_dir() {
			let dir = std::fs::read_dir(&p)
				.with_context(|| format!("Couldn't find directory {:?}", p.as_ref()))?;

			for file in dir {
				let path = file?.path();
				Self::format_file(&path)?;
			}
		} else {
			Self::format_file(path)?;
		}

		Ok(())
	}

	fn format_file(path: &Path) -> anyhow::Result<()> {
		let content = std::fs::read_to_string(path)?;
		let mut words: Vec<DictEntry> = serde_yaml::from_str(&content)
			.map_err(move |err| DictError::new(path.to_owned(), err, content))?;

		words.sort_by(|this, other| {
			this.strong.cmp(&other.strong).then_with(|| {
				let first_word = GrWord::new(this.word.clone()).unwrap().split_punctuation();
				let other_word = GrWord::new(other.word.clone()).unwrap().split_punctuation();
				first_word.word().cmp(other_word.word())
			})
		});

		let content = serde_yaml::to_string(&words)?;
		let content = content.replace("\n-", "\n\n-");
		std::fs::write(path, content)?;

		Ok(())
	}
}
