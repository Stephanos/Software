use crate::{Dict, DictEntry};

impl Dict {
	fn extract_words(&self, indices: Vec<usize>) -> Vec<&DictEntry> {
		indices.into_iter().map(|idx| &self.words[idx]).collect()
	}

	pub fn search<'a>(&'a self, word: &str) -> Vec<&'a DictEntry> {
		if word.is_ascii() {
			self.search_latin(word)
		} else {
			self.search_greek(word)
		}
	}

	pub fn search_latin<'a>(&'a self, word: &str) -> Vec<&'a DictEntry> {
		let word = word.replace("ng", "gg").replace("u", "ou");
		let indices = self.latin_indices.search(&word);
		Self::extract_words(self, indices)
	}

	pub fn search_greek<'a>(&'a self, word: &str) -> Vec<&'a DictEntry> {
		let indices = self.greek_indices.search(word);
		Self::extract_words(self, indices)
	}
}
