use anyhow::{Context, Result};
use simsearch::SimSearch;
use std::{collections::BTreeMap, ops::RangeInclusive, path::Path};
use text_util::{GrWord, GrWordComponents};

use crate::{Dict, DictEntry};

impl Dict {
	pub fn from_dir<P: AsRef<Path>>(p: P) -> Result<Self> {
		let dir = std::fs::read_dir(&p)
			.with_context(|| format!("Couldn't find directory {:?}", p.as_ref()))?;

		let mut words: Vec<DictEntry> = Vec::new();
		for file in dir {
			let content = std::fs::read(file?.path())?;
			words.extend(serde_yaml::from_slice::<Vec<DictEntry>>(&content)?);
		}

		Self::from_words(words)
	}

	fn words_from_file<P: AsRef<Path>>(p: P) -> Result<Vec<DictEntry>> {
		let content =
			std::fs::read(&p).with_context(|| format!("Couldn't find file {:?}", p.as_ref()))?;
		serde_yaml::from_slice(&content).with_context(|| format!("Error in file {:?}", p.as_ref()))
	}

	pub fn from_file<P: AsRef<Path>>(p: P) -> Result<Self> {
		let words: Vec<DictEntry> = Self::words_from_file(p)?;
		Self::from_words(words)
	}

	fn from_words(mut words: Vec<DictEntry>) -> Result<Self> {
		if words.is_empty() {
			return Ok(Self::default());
		}

		// Sort words by their strong number
		words.sort_by_key(|entry| entry.strong);

		// Calculate indices of strong words for strong lookup
		let mut strong_indices: Vec<RangeInclusive<usize>> =
			Vec::with_capacity(words.last().unwrap().strong as usize);

		for (idx, entry) in words.iter().enumerate() {
			if let Some(strong_entry) = strong_indices.get_mut(entry.strong as usize) {
				*strong_entry = RangeInclusive::new(*strong_entry.start(), idx);
			} else {
				strong_indices.push(RangeInclusive::new(idx, idx));
			}
		}

		let greek_words: Vec<GrWordComponents> = words
			.iter()
			.map(|word| GrWord::new(word.word.clone()).map(|word| word.split_punctuation()))
			.collect::<Result<Vec<_>, _>>()?;
		let greek_without_accents: Vec<String> = greek_words
			.iter()
			.map(|word| word.word().to_owned())
			.collect();
		let latin_words: Vec<String> = greek_words.iter().map(|word| word.to_latin()).collect();

		let mut greek_indices: SimSearch<usize> = SimSearch::new();
		let mut greek_indices_map = BTreeMap::new();
		for (idx, greek_word) in greek_without_accents.into_iter().enumerate() {
			greek_indices.insert(idx, &greek_word);
			greek_indices_map.insert(greek_word, idx);
		}

		let mut latin_indices: SimSearch<usize> = SimSearch::new();
		for (idx, latin_word) in latin_words.into_iter().enumerate() {
			latin_indices.insert(idx, &latin_word);
		}

		Ok(Self {
			words,
			strong_indices,
			greek_indices,
			greek_indices_map,
			latin_indices,
		})
	}

	/* pub(crate) fn lookup(&self, word: &str) -> Result<DictWord> {
		let word = word.to_lowercase();
		let result = self
			.data
			.get(&word)
			.ok_or_else(|| anyhow!("Couldn't find word `{word}` in dictionary"))?;
		Ok(result.clone())
	}

	pub(crate) fn lookup_word(&self, word: &str) -> Result<String> {
		Ok(self.lookup(word)?.meanings.into_iter().next().unwrap())
	}

	pub(crate) fn into_vec(self) -> Vec<(String, DictWord)> {
		self.data.into_iter().collect()
	}*/
}
