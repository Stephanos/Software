use std::ops::Bound;

use text_util::GrWord;

use crate::Dict;

impl Dict {
	pub fn estimate_gemoll_page(&self, word: &str) -> u16 {
		let word = GrWord::new(word.to_owned()).unwrap().split_punctuation();
		let word = word.word();

		const MIN: u16 = 1;
		const MAX: u16 = 879;

		let get_gemoll_number = |(_, idx): (&String, &usize)| -> u16 { self.words[*idx].gemoll };

		let mut cursor = self.greek_indices_map.lower_bound(Bound::Included(word));
		let prev = cursor.next().map(get_gemoll_number).unwrap_or(MIN);
		let next = cursor.next().map(get_gemoll_number).unwrap_or(MAX);

		// Very simple estimation, that can be improved...
		(prev + next) / 2
	}
}
