#[derive(serde::Serialize, serde::Deserialize)]
struct Verse {
	/// Name of the book.
	book: String,

	/// Number of the chapter.
	chapter: u8,

	/// Number of the verse.
	verse: u8,

	/// The version of the dictionary used for
	/// the latest translation.
	dict_version: Version,

	/// The version of the grammar guide used for
	/// the latest translation.
	grammar_version: Version,

	/// List of translators who worked on this verse.
	translators: Vec<Translator>,

	// date-translations: [May 7th];
	/// The difficulty of this verse.
	difficulty: Difficulty,

	/// The quality rating of this verse.
	rating: Rating,

	/// Translation contents.
	parts: Vec<Part>,
}

#[derive(serde::Serialize, serde::Deserialize)]
struct Version {
	number: String,
	hash: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
struct Translator {
	name: String,
	email: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
enum Difficulty {/*(very-hard - very-easy)*/}

#[derive(serde::Serialize, serde::Deserialize)]
enum Rating {} /*(very unhappy - very happy)*/

#[derive(serde::Serialize, serde::Deserialize)]
struct Part {
	translation: String,
	words: Vec<Word>,
}

#[derive(serde::Serialize, serde::Deserialize)]
struct Word {
	original_position: usize,
	position: usize,
	possible_translations: Vec<String>,
	form: WordForm,
	selected_translation_idx: usize,
}

#[derive(serde::Serialize, serde::Deserialize)]
struct WordForm {}

impl Verse {}
