use serde_json::Value;
use tokio::{fs, io};

use crate::{json::BibleInfo, Args};

static CACHE_DIR: &str = "bible-dl";

async fn fetch_chapter(args: &Args) -> io::Result<String> {
	let Args {
		translation,
		book,
		chapter,
	} = args;
	let book = book.to_string();

	let base_dirs = directories::BaseDirs::new().unwrap();
	let cache_dir = base_dirs.cache_dir().join(CACHE_DIR);

	// Create cache dir if it doesn't exist
	if !cache_dir.is_dir() {
		fs::create_dir(&cache_dir).await?;
	}

	// Create translation dir if it doesn't exist
	let translation_dir = cache_dir.join(translation);
	if !translation_dir.is_dir() {
		fs::create_dir(&translation_dir).await?;
	}

	// Create book dir if it doesn't exist
	let book_dir = translation_dir.join(&book);
	if !book_dir.is_dir() {
		fs::create_dir(&book_dir).await?;
	}

	// Download file if not already cached
	let file_path = book_dir.join(format!("{chapter}.html"));
	if !file_path.exists() {
		let client = reqwest::ClientBuilder::new()
			.redirect(reqwest::redirect::Policy::none())
			.build()
			.unwrap();
		let res = client
			.get(format!(
				"https://www.die-bibel.de/bibel/{translation}/{book}.{chapter}"
			))
			.send()
			.await
			.map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;

		let text = res
			.text()
			.await
			.map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;

		if !text.contains(&format!("{translation}/{book}.{chapter}")) {
			Err(io::Error::new(
				io::ErrorKind::InvalidData,
				format!("Chapter {book}.{chapter} does not appear in the HTML"),
			))
		} else {
			fs::write(file_path, &text).await?;
			Ok(text)
		}
	} else {
		fs::read_to_string(file_path).await
	}
}

pub async fn get_chapter(args: &Args) -> io::Result<Vec<(String, String)>> {
	let text = fetch_chapter(args).await?;

	const START_JSON: &str = "<script id=\"IBEP-main-state\" type=\"application/json\">";
	const END_JSON: &str = "</script>";

	let start_pos = text.find(START_JSON).unwrap() + START_JSON.len();
	let end_pos = text.rfind(END_JSON).unwrap();

	let text = &text[start_pos..end_pos];

	let value: Value = serde_json::from_str(text)?;
	let object = value.as_object().unwrap();
	for (key, value) in object {
		if key.contains("chapters") {
			let info = serde_json::from_value::<BibleInfo>(value.clone()).unwrap();
			return Ok(info.get_verses());
		}
	}

	Err(io::Error::new(
		io::ErrorKind::Other,
		"Couldn't find text data in the received HTML",
	))
}
