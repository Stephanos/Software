#[derive(Debug, serde::Deserialize)]
pub struct BibleInfo {
	data: Data,
}

impl BibleInfo {
	pub fn get_verses(self) -> Vec<(String, String)> {
		self.data.chapter.collapse_paragraphs()
	}
}

#[derive(Debug, serde::Deserialize)]
struct Data {
	chapter: Chapter,
}

#[derive(Debug, serde::Deserialize)]
struct Chapter {
	content: Vec<Paragraph>,
}

#[derive(Debug, serde::Deserialize)]
struct Paragraph {
	#[serde(default)]
	content: Vec<Verse>,
}

#[derive(Debug, serde::Deserialize)]
struct Verse {
	#[serde(rename = "type")]
	ty: String,
	#[serde(rename = "verseId", default)]
	verse_id: String,
	content: Option<serde_json::Value>,
}

impl Chapter {
	fn collapse_paragraphs(self) -> Vec<(String, String)> {
		self.content
			.into_iter()
			.flat_map(Paragraph::collapse_verses)
			.collect()
	}
}

impl Paragraph {
	fn collapse_verses(self) -> Vec<(String, String)> {
		let mut out = Vec::new();
		let mut verse_text = String::new();
		let mut verse_id = String::new();

		for verse in self.content {
			if verse.ty == "verse-number" {
				if !verse_text.is_empty() {
					out.push((verse_id, verse_text));
				}
				verse_text = String::new();
				verse_id = verse.verse_id;
			} else if verse.ty == "verse-text" {
				verse_text.push_str(
					verse
						.content
						.expect("Empty content")
						.as_str()
						.expect("Verse text is not a string"),
				);
			} else if verse.ty == "char" {
				let value = verse.content.expect("Empty content");
				let array = if let Some(array) = value.as_array() {
					array
				} else {
					continue;
				};
				let text: String = array
					.iter()
					.map(|v| v.as_object().expect("Char array content is not an object"))
					.filter(|obj| obj.get("type").unwrap() == "verse-text")
					.map(|obj| obj.get("content").unwrap().as_str().unwrap())
					.collect();
				verse_text.push_str(&text);
			}
		}

		// Push final verse if there is one
		if !verse_text.is_empty() {
			out.push((verse_id, verse_text));
		}

		out
	}
}
