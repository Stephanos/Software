//! A script to display the contents of the
//! bible from bibel.de in a console.

mod fetch;
mod json;

pub use fetch::get_chapter;

#[cfg(feature = "cli-args")]
use clap::Parser;

#[derive(Debug)]
#[cfg_attr(feature = "cli-args", derive(Parser))]
#[cfg_attr(feature = "cli-args", command(version, about, long_about = None))]
pub struct Args {
	/// Name of the translation, e. g. NA28 or LU17
	pub translation: String,
	/// Name of the book, e. g. JHN or LUK
	#[cfg_attr(feature = "cli-args", arg(value_parser = translate::BookId::try_from_str))]
	pub book: bible_id::BookId,
	/// Number of the chapter
	pub chapter: u8,
}
