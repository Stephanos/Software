//! A script to display the contents of the
//! bible from bibel.de in a console.

use clap::Parser;
use tokio::io;

#[tokio::main]
async fn main() -> io::Result<()> {
	let args = bible_dl::Args::parse();

	let verses = bible_dl::get_chapter(&args).await?;
	for (id, verse) in verses {
		println!("{id} {verse}")
	}

	Ok(())
}
