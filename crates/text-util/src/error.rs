use thiserror::Error;

#[derive(Debug, Error)]
pub enum TextError {
	#[error("Empty input")]
	EmptyInput,
	#[error("Invalid characters in word \"{0}\"")]
	InvalidCharacters(String),
}
