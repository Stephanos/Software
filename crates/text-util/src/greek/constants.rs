pub(crate) const SMALL_GREEK_LETTERS: [char; 25] = [
	'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'ς', 'σ',
	'τ', 'υ', 'φ', 'χ', 'ψ', 'ω',
];

pub(crate) const SMALL_GREEK_LETTERS_TO_LATIN_MAP: [&str; 25] = [
	"a", "b", "g", "d", "e", "z", "e", "th", "i", "k", "l", "m", "n", "x", "o", "p", "r", "s", "s",
	"t", "y", "ph", "ch", "ps", "o",
];

pub(crate) const LARGE_GREEK_LETTERS: [char; 24] = [
	'Α', 'Β', 'Γ', 'Δ', 'Ε', 'Ζ', 'Η', 'Θ', 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π', 'Ρ', 'Σ', 'Τ',
	'Υ', 'Φ', 'Χ', 'Ψ', 'Ω',
];

pub(crate) const LARGE_GREEK_LETTERS_TO_LATIN_MAP: [&str; 24] = [
	"A", "B", "G", "D", "E", "Z", "E", "Th", "I", "K", "L", "M", "N", "X", "O", "P", "R", "S", "T",
	"Y", "Ph", "Ch", "Ps", "O",
];

// Accents
pub(crate) const GRAVE: char = '\u{300}';
pub(crate) const ACUTE: char = '\u{301}';
pub(crate) const CIRCUMFLEX: char = '\u{342}';

// Breathings
pub(crate) const SPIRITUS_ASPER: char = '\u{314}';
pub(crate) const SPIRITUS_LENIS: char = '\u{313}';

// Subscripts
pub(crate) const IOTA_SUBSCRIPTUM: char = '\u{345}';

pub(crate) const GREEK_ACCENTS: [char; 6] = [
	GRAVE,
	ACUTE,
	CIRCUMFLEX,
	SPIRITUS_ASPER,
	SPIRITUS_LENIS,
	IOTA_SUBSCRIPTUM,
];
