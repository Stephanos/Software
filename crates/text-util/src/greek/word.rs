use unicode_normalization::UnicodeNormalization;
use unicode_segmentation::UnicodeSegmentation;

use crate::error::TextError;

use super::{
	latin,
	verify::{GrChar, GreekCharExt},
};

/// A string for ancient Greek words.
/// This type ensures additional guarantees on top of
/// [`String`] that make it easier and more consistent
/// to work with.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct GrWord {
	inner: String,
}

impl GrWord {
	pub fn new(word: String) -> Result<Self, TextError> {
		let filtered_word: Option<String> = word
			.nfd()
			.map(|c| c.is_greek_character().then_some(c))
			.collect();
		Ok(Self {
			inner: filtered_word.ok_or(TextError::InvalidCharacters(word))?,
		})
	}
}

#[derive(Debug, PartialEq, Eq)]
pub struct GrWordComponents {
	word: String,
	accents: String,
}

impl PartialOrd for GrWordComponents {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for GrWordComponents {
	fn cmp(&self, other: &Self) -> std::cmp::Ordering {
		// First compare the word, then the accents
		match self.word.cmp(&other.word) {
			core::cmp::Ordering::Equal => {}
			ord => return ord,
		}
		self.accents.cmp(&other.accents)
	}
}

impl TryFrom<String> for GrWord {
	type Error = TextError;

	fn try_from(value: String) -> Result<Self, Self::Error> {
		GrWord::new(value)
	}
}

impl GrWord {
	pub fn split_punctuation(&self) -> GrWordComponents {
		let mut word = String::new();
		let mut accents = String::new();

		for grapheme in self.inner.graphemes(true) {
			let GrChar {
				letter,
				accents: char_accents,
			} = GrChar::new(grapheme).unwrap();
			word.push(letter);
			accents.push_str(&char_accents);
		}

		GrWordComponents { word, accents }
	}
}

impl GrWordComponents {
	pub fn word(&self) -> &str {
		&self.word
	}

	pub fn accents(&self) -> &str {
		&self.accents
	}

	pub fn to_latin(&self) -> String {
		latin::map_to_latin(&self.word)
	}
}

#[cfg(test)]
mod test {
	use crate::GrWord;

	#[test]
	fn greek_word() {
		let _ = GrWord::new("ἄγγελος".into()).unwrap();
	}
}
