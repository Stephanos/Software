use super::constants::{
	LARGE_GREEK_LETTERS, LARGE_GREEK_LETTERS_TO_LATIN_MAP, SMALL_GREEK_LETTERS,
	SMALL_GREEK_LETTERS_TO_LATIN_MAP,
};

pub fn map_to_latin(input: &str) -> String {
	input
		.chars()
		.map(|ch| {
			if let Some(idx) = SMALL_GREEK_LETTERS.iter().position(|c| c == &ch) {
				SMALL_GREEK_LETTERS_TO_LATIN_MAP[idx]
			} else if let Some(idx) = LARGE_GREEK_LETTERS.iter().position(|c| c == &ch) {
				LARGE_GREEK_LETTERS_TO_LATIN_MAP[idx]
			} else {
				""
			}
		})
		.collect()
}
