use unicode_normalization::UnicodeNormalization;

use crate::error::TextError;

use super::constants::{GREEK_ACCENTS, LARGE_GREEK_LETTERS, SMALL_GREEK_LETTERS};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct GrChar {
	pub letter: char,
	pub accents: String,
}

impl GrChar {
	pub fn new(grapheme: &str) -> Result<Self, TextError> {
		let mut char_iter = grapheme.nfd();
		let letter = char_iter.next().ok_or(TextError::EmptyInput)?;
		let accents: String = char_iter.collect();

		if letter.is_greek_letter() && accents.chars().all(GreekCharExt::is_greek_accent) {
			Ok(Self { letter, accents })
		} else {
			Err(TextError::InvalidCharacters(grapheme.to_owned()))
		}
	}
}

impl TryFrom<String> for GrChar {
	type Error = TextError;

	fn try_from(grapheme: String) -> Result<Self, Self::Error> {
		GrChar::new(&grapheme)
	}
}

pub trait GreekCharExt: Copy {
	fn is_small_greek_letter(self) -> bool;

	fn is_large_greek_letter(self) -> bool;

	fn is_greek_letter(self) -> bool {
		self.is_small_greek_letter() || self.is_large_greek_letter()
	}

	fn is_greek_accent(self) -> bool;

	fn is_greek_punctuation(self) -> bool;

	fn is_greek_character(self) -> bool {
		self.is_greek_letter() || self.is_greek_accent()
	}
}

impl GreekCharExt for char {
	fn is_small_greek_letter(self) -> bool {
		SMALL_GREEK_LETTERS.contains(&self)
	}

	fn is_large_greek_letter(self) -> bool {
		LARGE_GREEK_LETTERS.contains(&self)
	}

	fn is_greek_accent(self) -> bool {
		GREEK_ACCENTS.contains(&self)
	}

	fn is_greek_punctuation(self) -> bool {
		todo!()
	}
}

#[cfg(test)]
mod test {
	use crate::greek::{
		constants::{ACUTE, CIRCUMFLEX, GRAVE, SPIRITUS_ASPER, SPIRITUS_LENIS},
		verify::{LARGE_GREEK_LETTERS, SMALL_GREEK_LETTERS},
	};

	use super::*;

	#[test]
	fn test_small_letters() {
		SMALL_GREEK_LETTERS.into_iter().for_each(|c| {
			assert!(
				c.is_small_greek_letter(),
				"{c} should be a small greek letter"
			)
		});

		SMALL_GREEK_LETTERS
			.into_iter()
			.for_each(|c| assert!(c.is_greek_letter(), "{c} should be a greek letter"));

		SMALL_GREEK_LETTERS.into_iter().for_each(|c| {
			assert!(
				!c.is_large_greek_letter(),
				"{c} shouldn't be a large greek letter"
			)
		});
	}

	#[test]
	fn test_large_letters() {
		LARGE_GREEK_LETTERS.into_iter().for_each(|c| {
			assert!(
				!c.is_small_greek_letter(),
				"{c} shouldn't be a small greek letter"
			)
		});

		LARGE_GREEK_LETTERS
			.into_iter()
			.for_each(|c| assert!(c.is_greek_letter(), "{c} should be a greek letter"));

		LARGE_GREEK_LETTERS.into_iter().for_each(|c| {
			assert!(
				c.is_large_greek_letter(),
				"{c} should be a large greek letter"
			)
		});
	}

	#[test]
	fn test_sorting() {
		fn to_gr_char(chars: [char; 2]) -> GrChar {
			chars.into_iter().collect::<String>().try_into().unwrap()
		}

		let mut chars: Vec<GrChar> = [
			['α', SPIRITUS_ASPER],
			['α', SPIRITUS_LENIS],
			['α', CIRCUMFLEX],
			['α', ACUTE],
			['α', GRAVE],
		]
		.into_iter()
		.map(to_gr_char)
		.collect();

		chars.sort();
		panic!("{chars:#?}");
	}
}
