//! Handling greek letters is slightly more complex than ASCII.
//! Depending on the context, you need words with or without punctuation
//! and comparing words might fail if they use different unicode representations.
//!
//! This crate helps with solving those problems by offering a elegant and
//! straight-forward API.
//!
//! # Sorting
//!
//! Greek words are typically sorted by their letters, ignoring accents
//! for the most part.
//! However, the accents are still important for distinguishing two words
//! with the same letters, but potentially different accents.
//!
//! To solve this problem, this crate provides a [`?`] type that implements
//! [`Ord`] in such a way, that sorting is easily possible.
//! Internally, each word is split into a word containing the letters and
//! one word with only placeholder letters and accents.
//! After this conversion, the word can be sorted by using its binary values
//! because the greek letters are properly sorted in the unicode standard
//! (even though there are gaps).

pub mod error;
mod greek;

pub use greek::*;
