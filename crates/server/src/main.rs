use std::sync::Arc;

use axum::{routing::get, Router};
use dict::{search, search_greek, search_latin, DictState};

mod dict;

pub type AppState = Arc<DictState>;

#[tokio::main]
async fn main() {
	let dict_state = Arc::new(DictState::new());
	let dict_routes = Router::new()
		.route("/latin/:word", get(search_latin))
		.route("/greek/:word", get(search_greek))
		.route("/search/:word", get(search));

	// build our application with a single route
	let app = Router::new()
		.route("/", get(|| async { "Hello, World!" }))
		.nest("/dict", dict_routes)
		.with_state(dict_state);

	// run our app with hyper, listening globally on port 7777
	let listener = tokio::net::TcpListener::bind("0.0.0.0:7777").await.unwrap();
	axum::serve(listener, app).await.unwrap();
}
