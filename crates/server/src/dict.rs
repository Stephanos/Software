use std::sync::Arc;

use axum::{
	extract::{Path, State},
	http::{header::CONTENT_TYPE, StatusCode},
	response::IntoResponse,
};
use dict::{Dict, DictEntry};

use crate::AppState;

pub struct DictState {
	dict: Dict,
}

impl DictState {
	pub fn new() -> Self {
		let dict = Dict::from_dir("../../dict").unwrap();
		Self { dict }
	}
}

fn words_to_response(words: Vec<&DictEntry>) -> impl IntoResponse {
	if let Ok(json) = serde_json::to_string(&words) {
		(
			// set status code
			StatusCode::OK,
			// headers with an array
			[(CONTENT_TYPE, "application/json")],
			json,
		)
	} else {
		(
			// set status code
			StatusCode::OK,
			// headers with an array
			[(CONTENT_TYPE, "application/json")],
			"".into(),
		)
	}
}

pub async fn search_latin(
	Path(word): Path<String>,
	State(state): State<Arc<DictState>>,
) -> impl IntoResponse {
	let words = state.dict.search_latin(&word);
	words_to_response(words)
}

pub async fn search_greek(
	Path(word): Path<String>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	let words = state.dict.search_greek(&word);
	words_to_response(words)
}

pub async fn search(Path(word): Path<String>, State(state): State<AppState>) -> impl IntoResponse {
	let words = state.dict.search(&word);
	words_to_response(words)
}
